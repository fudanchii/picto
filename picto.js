/*
 *  picto
 *  Web picture viewer.
 *  Nurahmadie <nurahmadie@gmail.com>
 *  2012
 *
 *  This script use ender (https://ender.no.de) with this build:
 *    ender build reqwest bonzo qwery bean drag domready jeesh morpheus
 *  
 *  Released under 2-clause BSD license.
 */

var picto = function (q) {
  var doms = $(q)
  ;
  doms.click(function (e) {
    var target = e.currentTarget
    ,   imgwidth = parseInt($("#"+target.id).attr("w"))
    ,   imgheight= parseInt($("#"+target.id).attr("h"))
    ,   imgalt = $("#"+target.id+" > img").attr("alt") 
    ,   fimg = $("<img/>").addClass("full-image")[0]

    ,   imgwidget= $("<div></div>").addClass("ui-widget")
    ,   proxydiv = $("<div></div>").addClass("proxy")
    ,   overlay = $("<div></div>").addClass("ui-overlay")
    ,   tagline = $("<div><h4>"+imgalt+"</h4></div>").addClass("ui-tagline")
    ,   resbutton = $("<div>resize</div>").addClass("resize-button")

    // viewport dimension for test against widget size
    ,   vw = $.viewport().width - 50
    ,   vh = $.viewport().height - 50

    // widget size
    ,   ww = (vw >= imgwidth) ? imgwidth : vw
    ,   wh = (vh >= imgheight) ? imgheight : vh

    // delta size
    ,   dw = (vw < imgwidth) ? imgwidth - vw : 0
    ,   dh = (vh < imgheight) ? imgheight - vh : 0

    // proxy size
    ,   pw = ww + dw * 2
    ,   ph = wh + dh * 2

    // scaled image size
    ,   sw = imgwidth
    ,   sh = imgheight
    ,   scaled = true
    ,   tclick = false

    // Image close defined as separate function
    , closefunc = function () {
        proxydiv.remove();
        tagline.remove();
        imgwidget.animate({
          duration: 400
        , width: 0
        , height: 0
        , "margin-top": "0px"
        , "margin-left": "0px"
        , complete: function () {
            imgwidget.remove();
            overlay.animate({
              duration: 400
            , opacity: 0
            , complete: function () {
                overlay.remove();
            }});
        }});
    }

    , addNoClick = function () {
      if ((!$(fimg).hasClass("noclick"))
          && /Firefox/.test(navigator.userAgent)) {
        $(fimg).addClass("noclick");
      }}
    
    , rmNoClick = function () {
      if ($(fimg).hasClass("noclick")) {
        $(fimg).removeClass("noclick");
        return;
      }
      closefunc();
    }

    , afterShow = function () {
      imgwidget.append(tagline);
      imgwidget.append(resbutton);
      imgwidget.append(proxydiv);
      $(fimg).drag().dragging(addNoClick)
             .container(".proxy").bind();
      $(fimg).click(rmNoClick);
      proxydiv.append(fimg);
    }

    , beforeShow = function () {
      $("body").append(imgwidget);
      imgwidget.animate({
        duration: 400
      , width: sw
      , height: sh
      , "margin-top": (0 - sh / 2) + "px"
      , "margin-left": (0 - sw / 2) + "px"
      , complete: afterShow
      });
    }

    , resizeClick = function () {
      if (ww == imgwidth && wh == imgheight)
        return;
      tagline.remove();
      resbutton.remove();
      proxydiv.remove();
      if (scaled) {
        proxydiv.width(pw);
        proxydiv.height(ph);
        proxydiv.css("left", (0 - dw) + "px");
        proxydiv.css("top",  (0 - dh) + "px");

        fimg.width = imgwidth;
        fimg.height= imgheight;
        $(fimg).css("left", dw + "px");
        $(fimg).css("top", dh + "px");

        imgwidget.animate({
          duration: 400
        , width: ww
        , height: wh
        , "margin-top": (0 - wh / 2) + "px"
        , "margin-left": (0 - ww / 2) + "px"
        , complete: afterShow
        });
        scaled = false;
      } else {
        proxydiv.width(sw);
        proxydiv.height(sh);
        proxydiv.css("left", "0px");
        proxydiv.css("top",  "0px");

        fimg.width = sw;
        fimg.height= sh;
        $(fimg).css("left", "0px");
        $(fimg).css("top", "0px");

        imgwidget.animate({
          duration: 400
        , width: sw
        , height: sh
        , "margin-top": (0 - sh / 2) + "px"
        , "margin-left": (0 - sw / 2) + "px"
        , complete: afterShow
        });
        scaled = true;
      }
    };

    tagline.click(function () {
      tclick = !tclick;
      tagline.animate({
        duration: 400,
        opacity: tclick ? 0 : 1
      });
    });

    resbutton.click(resizeClick);
 
    // Calculate scaled image size
    if (sw > vw) {
      sh = Math.max(Math.floor(vw * sh / sw), 1);
      sw = vw;
    }
    if (sh > vh) {
      sw = Math.max(Math.floor(vh * sw / sh), 1);
      sh = vh;
    }
  
    // Set image attribute
    fimg.width = sw;
    fimg.height= sh;
    $(fimg).css("left", "0px");
    $(fimg).css("top", "0px");
    fimg.src = target.href;
    
    // Set proxy screen attribute
    proxydiv.width(sw);
    proxydiv.height(sh);
    proxydiv.css("left",  "0px");
    proxydiv.css("top", "0px");

    // Set widget attribute
    imgwidget.width(0);
    imgwidget.height(0);
    imgwidget.css("margin-top", "0px");
    imgwidget.css("margin-left", "0px");

    // Show overlay screen first
    $("body").append(overlay);
    overlay.click(closefunc);

    // Animate overlay screen
    overlay.animate({
      duration: 400
    , opacity: 0.6
    , complete: beforeShow
    });

    e.preventDefault();
  });
};

$.domReady(function () {
  picto("a[rel=pict]");
});
